# Programmierprojekt Verteilte System

## Ports:
* Webserver: 4002
* NodeJS: 4000
* Adminer (DB Tool): 4003

## Wie man es startet:
```bash
sudo docker-compose up --remove-orphans 
```