async function getMessages(){
    //Lädt Daten für DOM Manipulation und bereitet DOM Manipulation vor
    var ul = document.getElementById('msg');
    var array = [];
    var li = new Array;
    //Sendet eine HTTP GET Anfrage an das Backend
await fetch("http://localhost:4000/api/messages/",{
    method: 'GET',
    credentials: 'same-origin'
    //Definiert Umgang mit der Antwort
}).then(Response => Response.json()).then(data => array = data);
//Wandelt die Antwort aus dem Array in DOM Elemente um
let  i = 1;
array.forEach(element =>{
    console.log(element);
  li[i] = document.createElement("li");
  li[i].innerHTML = element.id + ": " +element.username + " " + element.nachricht;
  ul.appendChild(li[i])
    i++;
})

}

async function postMessage(){
    //Lädt DOM Elemente
    var name = document.getElementById('1').value;
    var nachricht = document.getElementById('2').value;
    //Führt HTTP POST Request mit einem Body durch
    await fetch("http://localhost:4000/api/messages",{
        method: 'POST',
        headers: {
            'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        credentials: 'same-origin',
        body: JSON.stringify({'name':name,'nachricht':nachricht})
    });
    //Lädt Seite ohne Cache neu, sodass User die gepostete Nachricht sehen und
    // falls in der Zwischenzeit weitere Nachrichten gepostet / geändert wurden
    // so sieht er diese auch
    location.reload(true);
}

async function putMessage() {
    //Lädt die Elemente aus dem DOM
    let id,name,nachricht;
    id = document.getElementById(3).value;
    name = document.getElementById(4).value;
    nachricht = document.getElementById(5).value;
    //Führt einen HTTP PUT durch mit der oben geladen ID; Im Body
    // werden dann nachricht und name übergeben
    await fetch("http://localhost:4000/api/messages/" + id, {
        method: 'PUT',
        body: JSON.stringify ({
            'username': name,
            'nachricht': nachricht
        }),
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        }
    })
    .then(data => console.log(data));
    //Lädt Seite ohne Cache neu, sodass Updates angezeigt werden, wie diese 
    // in der Datenbank sind
    location.reload(true);
}

//Funktion um alle Nachrichten zu löschen
async function deleteMessages() {
    //Führt einen HTTP DELETE Request durch
    await fetch("http://localhost:4000/api/messages", {
    method: 'DELETE'
    });
    //Lädt Seite ohne Cache neu
    location.reload(true);
}

async function deleteMessage() {
    //Lädt DOM Element
    let id = document.getElementById(6).value;
    //Führt HTTP DELETE Request mit einer bestimmten ID durch
    await fetch("http://localhost:4000/api/messages/" +id, {
    method: 'DELETE'
    });
    //Lädt Seite ohne Cache neu
    location.reload(true);
}

