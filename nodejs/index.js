const express = require('express')
const app = express();
const db = require("./database")
const bodyParser = require('body-parser');

//Ermöglicht, dass man die URL auslesen kann
app.use(express.json());
app.use(express.urlencoded({
    extended:false
}));
//Ermöglicht, dass man den Body als JSON auslesen kann
app.use(bodyParser.urlencoded({ extended: true }));

//Access Control, sodass man von einem localhost:port auf localhost:andererport kommt
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
  });

//Restful Service für get
//Gibt alle Nachrichten aus der Datenbank zurück
app.get("/api/messages/",(req, res) =>{
    db.query("SELECT * FROM nachrichten", (err, result, field) =>{
        if(err) throw err;

        res.json(result)
    })
    
})

//Restful Service für post
//Fügt einen neuen Datensatz in die Datenbank ein
app.post("/api/messages",(req, res) =>{
    db.query("INSERT INTO nachrichten (username, nachricht) VALUES (?,?)",[req.body.name,req.body.nachricht],
    (err, result, req) =>{
        if(err) throw err;

        res.sendStatus(200)
    })
})

//Restful Service für Put
//Ändert über die ID eine Nachricht und deren Namen ab 
//In URL wird die ID übergeben und im body name und nachricht
app.put("/api/messages/:id", (req, res) =>{
    db.query("UPDATE nachrichten SET username = ?, nachricht =?  WHERE ID = ?",[req.body.username, req.body.nachricht, req.params.id], (err, result, fields)=>{
        if(err) throw err;
        res.sendStatus(200)
    })
})

//Restful Service für delete
//Löscht eine Nachricht aus der Datenbank
//Bestimmung mittels id; Übergabe in der URL
app.delete("/api/messages/:id",(req, res) =>{
    db.query("DELETE FROM nachrichten WHERE ID = ?" ,[req.params.id],(err, result, fields)=>{
        if(err) throw err;

        res.sendStatus(200);
    })
})

//Restful Service für delete
//Löscht alle Nachrichten aus der Datenbank
app.delete("/api/messages",(req, res)=>{
    db.query("DELETE FROM nachrichten",(err, result, fields) =>{
        if(err) throw err;

        res.sendStatus(200)
    })
})
//Führt den NodeJS Server auf dem Port 8080 (intern) aus
app.listen(8080, () =>{
    console.log("Server is running on Port 8080")
})