const mysql = require("mysql");

//Exportiert die Datenbankverbindung, sodass andere Klassen damit arbeiten können
module.exports = mysql.createConnection({
    host:"mysql",
    user:"root",
    password:"root",
    database:"messages"
})
